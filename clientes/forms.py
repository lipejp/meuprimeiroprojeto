from django.forms import ModelForm
from .models import *


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ['frist_name', 'last_name', 'age', 'salary', 'bio', 'photo']
